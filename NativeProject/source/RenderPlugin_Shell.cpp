/*
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#include "PlatformBase.h"
#include "RenderAPI.h"

#include "RenderPlugin.h"

#define _CRT_SECURE_NO_WARNINGS
#include "windows.h"
#include "stdio.h"


// macro for bulk managment of the interface functions.
// declare the function pointer in NativePlugin.h
// and implement the wrapper in this file.
#define LIST_OF_FUNCS \
X(UnityPluginLoad) \
X(UnityPluginUnload) \
X(InitializePlugin) \
X(ShutdownPlugin) \
X(GetRenderEventFunc) \
X(SetViewProjectionMatrixes) \


static void LoadRealLibrary();
static void FreeRealLibrary();

static IUnityInterfaces* s_UnityInterfaces = nullptr;

static HWND s_Console = NULL;
static HMODULE s_hLibrary = NULL;

// declare all the function pointers
#define X(x) static FN##x s_fn##x = nullptr;
LIST_OF_FUNCS
#undef X


//------------------------------------------------------------------------------
extern "C" void	UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API UnityPluginLoad(IUnityInterfaces* unityInterfaces)
{
	s_UnityInterfaces = unityInterfaces;
}


//------------------------------------------------------------------------------
extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API UnityPluginUnload()
{
	s_UnityInterfaces = nullptr;
}


//------------------------------------------------------------------------------
extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API InitializePlugin()
{
	s_Console = GetConsoleWindow();
	if (s_Console == NULL)
	{
		AllocConsole();
		s_Console = GetConsoleWindow();		
	}

	if (s_Console != NULL)
	{
		freopen("CONIN$", "r", stdin);
		freopen("CONOUT$", "w", stdout);
		freopen("CONOUT$", "w", stderr);
	}

	LoadRealLibrary();

	if (s_fnUnityPluginLoad != nullptr)
	{
		s_fnUnityPluginLoad(s_UnityInterfaces);
	}

	if (s_fnInitializePlugin != nullptr)
	{
		s_fnInitializePlugin();
	}
}


//------------------------------------------------------------------------------
extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API ShutdownPlugin()
{
	if (s_fnShutdownPlugin != nullptr )
		s_fnShutdownPlugin();

	if (s_fnUnityPluginUnload != nullptr)
		s_fnUnityPluginUnload();

	FreeRealLibrary();

	if (s_Console != NULL)
	{
		FreeConsole();
		s_Console = NULL;
	}
}


//------------------------------------------------------------------------------
extern "C" UnityRenderingEvent UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API GetRenderEventFunc()
{
	if (s_fnGetRenderEventFunc != nullptr)
	{
		return s_fnGetRenderEventFunc();
	}

	return nullptr;
}


//------------------------------------------------------------------------------
extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SetViewProjectionMatrixes(NativeMatrix44* view, NativeMatrix44* projection)
{
	if (s_fnSetViewProjectionMatrixes != nullptr)
	{
		s_fnSetViewProjectionMatrixes(view, projection);
	}
}


//------------------------------------------------------------------------------
static void LoadRealLibrary()
{
	FreeRealLibrary();

	printf("load real library\n");

	const char* newLibraryFilename = "new_NativePlugin.dll";
	const char* realLibraryFilename = "NativePlugin.dll";

	MoveFileEx(newLibraryFilename, realLibraryFilename, MOVEFILE_REPLACE_EXISTING | MOVEFILE_WRITE_THROUGH);

	s_hLibrary = LoadLibrary(realLibraryFilename);
	if (s_hLibrary == NULL)
	{
		printf("dll load fail\n");
		return;
	}

	// Get all the functions from the real library
#define X(x) s_fn##x = (FN##x)GetProcAddress(s_hLibrary, #x); if( s_fn##x == nullptr ) { printf("Failed to find function in real library: "#x"\n" ); }
	LIST_OF_FUNCS
#undef X
}


//------------------------------------------------------------------------------
static void FreeRealLibrary()
{
	printf("free real library\n");

	// null out all the function pointers
#define X(x) s_fn##x = nullptr;
	LIST_OF_FUNCS
#undef X

	if (s_hLibrary != NULL)
	{
		FreeLibrary(s_hLibrary);
		s_hLibrary = NULL;
	}
}
*/