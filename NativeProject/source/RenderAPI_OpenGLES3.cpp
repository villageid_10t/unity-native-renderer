/*
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#include "RenderAPI.h"
#include "PlatformBase.h"
#include "RenderPlugin.h"


#if SUPPORT_OPENGL_ES3


#include <stdio.h>
#include <assert.h>
#include "GLEW/glew.h"
#include "glm/glm.hpp"
#include "glm/ext.hpp"

using namespace glm;



static void CheckOpenGLError(const char* stmt, const char* fname, int line)
{
	GLenum err = glGetError();
	if (err != GL_NO_ERROR)
	{
		printf("OpenGL error %08x, at %s:%i - for %s\n", err, fname, line, stmt);
		abort();
	}
}

#ifdef _DEBUG
#define GL_CHECK(stmt) do { \
            stmt; \
            CheckOpenGLError(#stmt, __FILE__, __LINE__); \
        } while (0)
#else
#define GL_CHECK(stmt) stmt
#endif


enum VertexInputs
{
	kVertexInputPosition = 0,
	kVertexInputColor = 1
};



// Simple vertex shader source
#define VERTEX_SHADER_SRC(ver, attr, varying)						\
	ver																\
	attr " highp vec3 pos;\n"										\
	attr " lowp vec4 color;\n"										\
	"\n"															\
	varying " lowp vec4 ocolor;\n"									\
	"\n"															\
	"uniform highp mat4 worldMatrix;\n"								\
	"uniform highp mat4 projMatrix;\n"								\
	"\n"															\
	"void main()\n"													\
	"{\n"															\
	"	gl_Position = (projMatrix * worldMatrix) * vec4(pos,1);\n"	\
	"	ocolor = color;\n"											\
	"}\n"															\

static const char* kGlesVProgTextGLES3 = VERTEX_SHADER_SRC("#version 300 es\n", "in", "out");
#undef VERTEX_SHADER_SRC


// Simple fragment shader source
#define FRAGMENT_SHADER_SRC(ver, varying, outDecl, outVar)	\
	ver												\
	outDecl											\
	varying " lowp vec4 ocolor;\n"					\
	"\n"											\
	"void main()\n"									\
	"{\n"											\
	"	" outVar " = ocolor;\n"						\
	"}\n"											\

static const char* kGlesFShaderTextGLES3 = FRAGMENT_SHADER_SRC("#version 300 es\n", "in", "out lowp vec4 fragColor;\n", "fragColor");
#undef FRAGMENT_SHADER_SRC






//------------------------------------------------------------------------------
// OpenGL ES3 implementation of RenderAPI.
//------------------------------------------------------------------------------
class RenderAPI_OpenGLES3 : public RenderAPI
{
private:
	UnityGfxRenderer m_APIType;
	GLuint m_VertexShader;
	GLuint m_FragmentShader;
	GLuint m_Program;
	GLuint m_VertexArray;
	GLuint m_VertexBuffer;
	int m_UniformWorldMatrix;
	int m_UniformProjMatrix;

public:
	//--------------------------------------------------------------------------
	RenderAPI_OpenGLES3(UnityGfxRenderer apiType)
		: m_APIType(apiType)
	{}


	//--------------------------------------------------------------------------
	virtual ~RenderAPI_OpenGLES3()
	{}


	//--------------------------------------------------------------------------
	virtual void ProcessDeviceEvent(UnityGfxDeviceEventType type, IUnityInterfaces* interfaces)
	{
		printf("RenderAPI_OpenGLES3::ProcessDeviceEvent\n");


		if (type == kUnityGfxDeviceEventInitialize)
		{
			CreateResources();
		}
		else if (type == kUnityGfxDeviceEventShutdown)
		{
			//@TODO: release resources
		}
	}

	enum struct ERenderEvent : uint32_t
	{
		CreateResources,
		RenderScene,
	};

	//--------------------------------------------------------------------------
	virtual void OnRenderEvent(int e )
	{
		switch( e)
		{
		case ERenderEvent::CreateResources:
			//CreateResources();
			break;
		case ERenderEvent::RenderScene:
			RenderScene();
			break;
		default:
			break;
		}
	}


	//--------------------------------------------------------------------------
	virtual void SetViewProjectionMatrixes(const NativeMatrix44& view, const NativeMatrix44& projection)
	{
		mat3x3 m;
		memcpy(&m, &view, sizeof(m));

		
		Print(view, projection); printf("\n");

	}


	//virtual bool GetUsesReverseZ() { return false; }

	//virtual void DrawSimpleTriangles(const float worldMatrix[16], int triangleCount, const void* verticesFloat3Byte4);

	//virtual void* BeginModifyTexture(void* textureHandle, int textureWidth, int textureHeight, int* outRowPitch);
	//virtual void EndModifyTexture(void* textureHandle, int textureWidth, int textureHeight, int rowPitch, void* dataPtr);

	//virtual void* BeginModifyVertexBuffer(void* bufferHandle, size_t* outBufferSize);
	//virtual void EndModifyVertexBuffer(void* bufferHandle);

private:
	//--------------------------------------------------------------------------
	void CreateResources()
	{
		// Create shaders
		//if (m_APIType == kUnityGfxRendererOpenGLES20)
		//{
		//	m_VertexShader = CreateShader(GL_VERTEX_SHADER, kGlesVProgTextGLES2);
		//	m_FragmentShader = CreateShader(GL_FRAGMENT_SHADER, kGlesFShaderTextGLES2);
		//}
		//else 
		//if (m_APIType == kUnityGfxRendererOpenGLES30)
		//{			
			//m_VertexShader = CreateShader(GL_VERTEX_SHADER, kGlesVProgTextGLES3);
			//m_FragmentShader = CreateShader(GL_FRAGMENT_SHADER, kGlesFShaderTextGLES3);
		//}


		// Link shaders into a program and find uniform locations
		//GL_CHECK(m_Program = glCreateProgram());
		//GL_CHECK(glBindAttribLocation(m_Program, kVertexInputPosition, "pos"));
		//GL_CHECK(glBindAttribLocation(m_Program, kVertexInputColor, "color"));
		//GL_CHECK(glAttachShader(m_Program, m_VertexShader));
		//GL_CHECK(glAttachShader(m_Program, m_FragmentShader));
		//GL_CHECK(glLinkProgram(m_Program));

		//GLint status = 0;
		//GL_CHECK(glGetProgramiv(m_Program, GL_LINK_STATUS, &status));
		//

		//GL_CHECK(m_UniformWorldMatrix = glGetUniformLocation(m_Program, "worldMatrix"));
		//GL_CHECK(m_UniformProjMatrix = glGetUniformLocation(m_Program, "projMatrix"));

		//// Create vertex buffer
		//GL_CHECK(glGenBuffers(1, &m_VertexBuffer));
		//GL_CHECK(glBindBuffer(GL_ARRAY_BUFFER, m_VertexBuffer));
		//GL_CHECK(glBufferData(GL_ARRAY_BUFFER, 1024, NULL, GL_STREAM_DRAW));			
	}


	//--------------------------------------------------------------------------
	static GLuint CreateShader(GLenum type, const char* sourceText)
	{
		GLuint ret;
		GL_CHECK(ret = glCreateShader(type));
		//GL_CHECK(glShaderSource(ret, 1, &sourceText, NULL));
		//GL_CHECK(glCompileShader(ret));
		return ret;
	}


	//--------------------------------------------------------------------------
	void RenderScene()
	{
		//printf("RenderAPI_OpenGLES3::OnRenderEvent\n");
		struct MyVertex
		{
			float x, y, z;
			unsigned int color;
		};
		MyVertex verts[3] =
		{
			{ -0.5f, -0.25f,  0, 0xFFff0000 },
			{ 0.5f, -0.25f,  0, 0xFF00ff00 },
			{ 0,     0.5f ,  0, 0xFF0000ff },
		};

		// Transformation matrix: rotate around Z axis based on time.
		//float phi = 0.0f;//g_Time; // time set externally from Unity script
		//float cosPhi = cosf(phi);
		//float sinPhi = sinf(phi);
		

		float worldMatrix[16] =
		{
			1,0,0,0,
			0,1,0,0,
			0,0,1,0,
			0,0,0,1,
		};

		DrawSimpleTriangles((float*)&worldMatrix[0], 1, verts);
	}
	

	//--------------------------------------------------------------------------
	void DrawSimpleTriangles(const float worldMatrix[16], int triangleCount, const void* verticesFloat3Byte4)
	{
		// Set basic render state
		glDisable(GL_CULL_FACE);
		glDisable(GL_BLEND);
		glDepthFunc(GL_LEQUAL);
		glEnable(GL_DEPTH_TEST);
		glDepthMask(GL_FALSE);

		// Tweak the projection matrix a bit to make it match what identity projection would do in D3D case.
		float projectionMatrix[16] = {
			1,0,0,0,
			0,1,0,0,
			0,0,2,0,
			0,0,-1,1,
		};

		// Setup shader program to use, and the matrices
		glUseProgram(m_Program);
		glUniformMatrix4fv(m_UniformWorldMatrix, 1, GL_FALSE, worldMatrix);
		glUniformMatrix4fv(m_UniformProjMatrix, 1, GL_FALSE, projectionMatrix);

		// Bind a vertex buffer, and update data in it
		const int kVertexSize = 12 + 4;
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, m_VertexBuffer);
		glBufferSubData(GL_ARRAY_BUFFER, 0, kVertexSize * triangleCount * 3, verticesFloat3Byte4);

		// Setup vertex layout
		glEnableVertexAttribArray(kVertexInputPosition);
		glVertexAttribPointer(kVertexInputPosition, 3, GL_FLOAT, GL_FALSE, kVertexSize, (char*)NULL + 0);
		glEnableVertexAttribArray(kVertexInputColor);
		glVertexAttribPointer(kVertexInputColor, 4, GL_UNSIGNED_BYTE, GL_TRUE, kVertexSize, (char*)NULL + 12);

		// Draw
		glDrawArrays(GL_TRIANGLES, 0, triangleCount * 3);
	}

};


RenderAPI* CreateRenderAPI_OpenGLES3(UnityGfxRenderer apiType)
{
	printf("CreateRenderAPI_OpenGLES3\n");

	return new RenderAPI_OpenGLES3(apiType);
}





//void* RenderAPI_OpenGLES3::BeginModifyTexture(void* textureHandle, int textureWidth, int textureHeight, int* outRowPitch)
//{
//	const int rowPitch = textureWidth * 4;
//	// Just allocate a system memory buffer here for simplicity
//	unsigned char* data = new unsigned char[rowPitch * textureHeight];
//	*outRowPitch = rowPitch;
//	return data;
//}


//void RenderAPI_OpenGLES3::EndModifyTexture(void* textureHandle, int textureWidth, int textureHeight, int rowPitch, void* dataPtr)
//{
//	GLuint gltex = (GLuint)(size_t)(textureHandle);
//	// Update texture data, and free the memory buffer
//	glBindTexture(GL_TEXTURE_2D, gltex);
//	glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, textureWidth, textureHeight, GL_RGBA, GL_UNSIGNED_BYTE, dataPtr);
//	delete[](unsigned char*)dataPtr;
//}

//void* RenderAPI_OpenGLES3::BeginModifyVertexBuffer(void* bufferHandle, size_t* outBufferSize)
//{
//#	if SUPPORT_OPENGL_ES
//	return 0;
//#	else
//	glBindBuffer(GL_ARRAY_BUFFER, (GLuint)(size_t)bufferHandle);
//	GLint size = 0;
//	glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &size);
//	*outBufferSize = size;
//	void* mapped = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
//	return mapped;
//#	endif
//}


//void RenderAPI_OpenGLES3::EndModifyVertexBuffer(void* bufferHandle)
//{
//#	if !SUPPORT_OPENGL_ES
//	glBindBuffer(GL_ARRAY_BUFFER, (GLuint)(size_t)bufferHandle);
//	glUnmapBuffer(GL_ARRAY_BUFFER);
//#	endif
//}

#endif // #if SUPPORT_OPENGL_UNIFIED
*/