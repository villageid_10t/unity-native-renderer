//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
#include "PlatformBase.h"
#include "RenderAPI.h"

#include "RenderPlugin.h"

#include <windows.h>
#include <assert.h>
#include <math.h>
#include <vector>


// -----------------------------------------------------------------------------
// UnitySetInterfaces
// -----------------------------------------------------------------------------
// wrapper for initialization on WebGL because the dll doesn't exist so UnityPluginLoad doesn't get called
#if UNITY_WEBGL 
typedef void	(UNITY_INTERFACE_API * PluginLoadFunc)(IUnityInterfaces* unityInterfaces);
typedef void	(UNITY_INTERFACE_API * PluginUnloadFunc)();

extern "C" void	UnityRegisterRenderingPlugin(PluginLoadFunc loadPlugin, PluginUnloadFunc unloadPlugin);
#endif

static void UNITY_INTERFACE_API OnGraphicsDeviceEvent(UnityGfxDeviceEventType eventType);
static void UNITY_INTERFACE_API OnRenderEvent(int eventID);


static IUnityInterfaces* s_UnityInterfaces = NULL;
static IUnityGraphics* s_Graphics = NULL;

static UnityGfxRenderer s_DeviceType = kUnityGfxRendererNull;
static RenderAPI* s_CurrentAPI = NULL;

static HWND s_Console = NULL;


// -----------------------------------------------------------------------------
extern "C" void	UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API UnityPluginLoad(IUnityInterfaces* unityInterfaces)
{
	printf("UnityPluginLoad\n");

	s_UnityInterfaces = unityInterfaces;
	if( s_UnityInterfaces != nullptr )
	{
		s_Graphics = s_UnityInterfaces->Get<IUnityGraphics>();
	}

	if( s_Graphics != nullptr )
	{
		s_Graphics->RegisterDeviceEventCallback(OnGraphicsDeviceEvent);
	}

	// Run OnGraphicsDeviceEvent(initialize) manually on plugin load
	OnGraphicsDeviceEvent(kUnityGfxDeviceEventInitialize);
}


// -----------------------------------------------------------------------------
extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API UnityPluginUnload()
{
	printf("UnityPluginUnload\n");

	if( s_Graphics != nullptr )
	{
		s_Graphics->UnregisterDeviceEventCallback(OnGraphicsDeviceEvent);
	}
}


//// -----------------------------------------------------------------------------
//extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API InitializePlugin()
//{	
////#if UNITY_WEBGL
////	UnityRegisterRenderingPlugin(UnityPluginLoad, UnityPluginUnload);
////#endif
//
//	printf("InitializePlugin\n");
//}


//// -----------------------------------------------------------------------------
//extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API ShutdownPlugin()
//{
//	printf("ShutdownPlugin\n");
//}


// -----------------------------------------------------------------------------
extern "C" UnityRenderingEvent UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API GetRenderEventFunc()
{
	//printf("GetRenderEventFunc\n");

	return OnRenderEvent;
}


////------------------------------------------------------------------------------
//extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API SetViewProjectionMatrixes( NativeMatrix44* view, NativeMatrix44* projection)
//{
//	//printf("SetViewAndProjection\n");
//
//	if (view == nullptr || projection == nullptr)
//	{
//		printf("SetViewAndProjection null matrix");
//	}
//
//	s_CurrentAPI->SetViewProjectionMatrixes(*view, *projection);
//}


// -----------------------------------------------------------------------------
static void UNITY_INTERFACE_API OnGraphicsDeviceEvent(UnityGfxDeviceEventType eventType)
{
	printf("OnGraphicsDeviceEvent\n");

	// Create graphics API implementation upon initialization
	if (eventType == kUnityGfxDeviceEventInitialize)
	{
		s_Console = GetConsoleWindow();
		if (s_Console == NULL)
		{
			AllocConsole();
			s_Console = GetConsoleWindow();
		}

		if (s_Console != NULL)
		{
			freopen("CONIN$", "r", stdin);
			freopen("CONOUT$", "w", stdout);
			freopen("CONOUT$", "w", stderr);
		}

		assert(s_CurrentAPI == nullptr);
		s_DeviceType = s_Graphics->GetRenderer();
		s_CurrentAPI = CreateRenderAPI(s_DeviceType);

		AllocConsole();
	}

	// Let the implementation process the device related events
	if (s_CurrentAPI)
	{
		s_CurrentAPI->ProcessDeviceEvent(eventType, s_UnityInterfaces);
	}

	// Cleanup graphics API implementation upon shutdown
	if (eventType == kUnityGfxDeviceEventShutdown)
	{
		delete s_CurrentAPI;
		s_CurrentAPI = nullptr;
		s_DeviceType = kUnityGfxRendererNull;
	}
}


// -----------------------------------------------------------------------------
static void UNITY_INTERFACE_API OnRenderEvent(int eventID)
{
	//printf("OnRenderEvent\n");

	if (s_CurrentAPI != nullptr)
	{
		s_CurrentAPI->OnRenderEvent(eventID);
	}
}

extern "C" void UNITY_INTERFACE_EXPORT UNITY_INTERFACE_API InitializePlugin()
{}







//// -----------------------------------------------------------------------------
//void Print(NativeMatrix44 a)
//{
//	printf("%7.4f,%8.4f,%8.4f,%8.4f\n"\
//		"%7.4f,%8.4f,%8.4f,%8.4f\n"\
//		"%7.4f,%8.4f,%8.4f,%8.4f\n"\
//		"%7.4f,%8.4f,%8.4f,%8.4f\n",
//		a.m_00, a.m_10, a.m_20, a.m_30,
//		a.m_01, a.m_11, a.m_21, a.m_31,
//		a.m_02, a.m_12, a.m_22, a.m_32,
//		a.m_03, a.m_13, a.m_23, a.m_33);
//}
//
//
//// -----------------------------------------------------------------------------
//void Print(NativeMatrix44 a, NativeMatrix44 b)
//{
//	printf("%7.4f,%8.4f,%8.4f,%8.4f    %7.4f,%8.4f,%8.4f,%8.4f\n"\
//		"%7.4f,%8.4f,%8.4f,%8.4f    %7.4f,%8.4f,%8.4f,%8.4f\n"\
//		"%7.4f,%8.4f,%8.4f,%8.4f    %7.4f,%8.4f,%8.4f,%8.4f\n"\
//		"%7.4f,%8.4f,%8.4f,%8.4f    %7.4f,%8.4f,%8.4f,%8.4f\n",
//		a.m_00, a.m_10, a.m_20, a.m_30, b.m_00, b.m_10, b.m_20, b.m_30,
//		a.m_01, a.m_11, a.m_21, a.m_31, b.m_01, b.m_11, b.m_21, b.m_31,
//		a.m_02, a.m_12, a.m_22, a.m_32, b.m_02, b.m_12, b.m_22, b.m_32,
//		a.m_03, a.m_13, a.m_23, a.m_33, b.m_03, b.m_13, b.m_23, b.m_33);
//}
