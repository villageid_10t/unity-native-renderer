#include "RenderAPI.h"
#include "PlatformBase.h"
#include "Unity/IUnityGraphics.h"


RenderAPI* CreateRenderAPI(UnityGfxRenderer apiType)
{
/*
#	if SUPPORT_D3D11
	if (apiType == kUnityGfxRendererD3D11)
	{
		extern RenderAPI* CreateRenderAPI_D3D11();
		return CreateRenderAPI_D3D11();
	}
#	endif // if SUPPORT_D3D11

#	if SUPPORT_D3D9
	if (apiType == kUnityGfxRendererD3D9)
	{
		extern RenderAPI* CreateRenderAPI_D3D9();
		return CreateRenderAPI_D3D9();
	}
#	endif // if SUPPORT_D3D9

#	if SUPPORT_D3D12
	if (apiType == kUnityGfxRendererD3D12)
	{
		extern RenderAPI* CreateRenderAPI_D3D12();
		return CreateRenderAPI_D3D12();
	}
#	endif // if SUPPORT_D3D9
*/	
#	if SUPPORT_OPENGL_CORE
	if (apiType == kUnityGfxRendererOpenGLCore)
	{
		extern RenderAPI* CreateRenderAPI_OpenGLCore(UnityGfxRenderer apiType);
		return CreateRenderAPI_OpenGLCore(apiType);
	}
#	endif // if SUPPORT_OPENGL_CORE
/*
#	if SUPPORT_OPENGL_ES3
	if (apiType == kUnityGfxRendererOpenGLES30)
	{
		extern RenderAPI* CreateRenderAPI_OpenGLES3(UnityGfxRenderer apiType);
		return CreateRenderAPI_OpenGLES3(apiType);
	}
#	endif // if SUPPORT_OPENGL_ES3

#	if SUPPORT_OPENGL_ES2
	if (apiType == kUnityGfxRendererOpenGLES20)
	{
		return null;
	}
#	endif // if SUPPORT_OPENGL_ES2

#	if SUPPORT_OPENGL_LEGACY
	if (apiType == kUnityGfxRendererOpenGL)
	{
		extern RenderAPI* CreateRenderAPI_OpenGL2();
		return CreateRenderAPI_OpenGL2();
	}
#	endif // if SUPPORT_OPENGL_LEGACY

#	if SUPPORT_METAL
	if (apiType == kUnityGfxRendererMetal)
	{
		extern RenderAPI* CreateRenderAPI_Metal();
		return CreateRenderAPI_Metal();
	}
#	endif // if SUPPORT_METAL
*/
	// Unknown or unsupported graphics API
	return nullptr;
}
