﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

using UnityEngine;


//[StructLayout(LayoutKind.Sequential, Pack = 1)]
//public struct NativeVector2
//{
//    float m_X;
//    float m_Y;
//};


//[StructLayout(LayoutKind.Sequential, Pack = 1)]
//public struct NativeVector3
//{
//    float m_X;
//    float m_Y;
//    float m_Z;
//};


//[StructLayout(LayoutKind.Sequential, Pack = 1)]
//public struct NativeVector4
//{
//    float m_X;
//    float m_Y;
//    float m_Z;
//    float m_W;
//};


//[StructLayout(LayoutKind.Sequential, Pack = 1)]
//public struct NativeQuaternion
//{
//    float m_X;
//    float m_Y;
//    float m_Z;
//    float m_W;
//};


//[StructLayout(LayoutKind.Sequential, Pack = 1)]
//public struct NativeColor32
//{
//    char m_R;
//    char m_G;
//    char m_B;
//    char m_A;
//};


//// column major
//[StructLayout(LayoutKind.Sequential, Pack = 1)]
//public struct NativeMatrix33
//{
//    float m_00;
//    float m_01;
//    float m_02;

//    float m_10;
//    float m_11;
//    float m_12;

//    float m_20;
//    float m_21;
//    float m_22;
//};


//// column major
//[StructLayout(LayoutKind.Sequential, Pack = 1)]
//public struct NativeMatrix44
//{
//    float m_00;
//    float m_01;
//    float m_02;
//    float m_03;

//    float m_10;
//    float m_11;
//    float m_12;
//    float m_13;

//    float m_20;
//    float m_21;
//    float m_22;
//    float m_23;

//    float m_30;
//    float m_31;
//    float m_32;
//    float m_33;
//};




public class RenderPlugin
{
#if (UNITY_IPHONE || UNITY_WEBGL) && !UNITY_EDITOR
    const string LIBRARY_NAME = "__Internal";
#elif UNITY_EDITOR
    const string LIBRARY_NAME = "RenderPlugin";//"NativePlugin_Shell";
#else
    const string LIBRARY_NAME = "NativePlugin";
#endif

    //public enum ERenderEvent : uint
    //{
    //    CreateResources,
    //    RenderScene,
    //}

    [DllImport(LIBRARY_NAME)]
    public static extern void InitializePlugin();

    //[DllImport(LIBRARY_NAME)]
    //public static extern void ShutdownPlugin();

    [DllImport(LIBRARY_NAME)]
    public static extern System.IntPtr GetRenderEventFunc();

    //[DllImport(LIBRARY_NAME)]
    //public static extern void SetViewProjectionMatrixes( [In] Matrix4x4 view, [In] Matrix4x4 projection );    
}
