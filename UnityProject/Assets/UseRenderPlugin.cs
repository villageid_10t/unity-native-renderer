using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;


public class UseRenderPlugin : MonoBehaviour
{
    Coroutine m_COInvokeNativeRender = null;

    private void Start()
    {
    }

    void OnEnable()
	{
        //RenderPlugin.InitializePlugin();
        
        m_COInvokeNativeRender = StartCoroutine( COCallPluginAtEndOfFrames() );

        //GL.IssuePluginEvent(RenderPlugin.GetRenderEventFunc(), 0);
    }


    private void OnDisable()
    {
        StopCoroutine( m_COInvokeNativeRender );

        //NativePlugin.ShutdownPlugin();
    }


    private void Update()
    {}


	private IEnumerator COCallPluginAtEndOfFrames()
	{
		while (true) {
			// Wait until all frame rendering is done
			yield return new WaitForEndOfFrame();

            // Set time for the plugin
            //SetTimeFromUnity (Time.timeSinceLevelLoad);

            Matrix4x4 v = new Matrix4x4();
            Matrix4x4 p = new Matrix4x4();

            v.SetColumn(0, new Vector4(0.0f, 1.0f, 2.0f, 3.0f));
            v.SetColumn(1, new Vector4(4.0f, 5.0f, 6.0f, 7.0f));
            v.SetColumn(2, new Vector4(8.0f, 9.0f, 10.0f, 11.0f));
            v.SetColumn(3, new Vector4(12.0f, 13.0f, 14.0f, 15.0f));
                        
            //NativePlugin.SetViewProjectionMatrixes(v,p);


            // Issue a plugin event with arbitrary integer identifier.
            // The plugin can distinguish between different
            // things it needs to do based on this ID.
            // For our simple plugin, it does not matter which ID we pass here.
            
            GL.IssuePluginEvent(RenderPlugin.GetRenderEventFunc(), 1 );
		}

        yield break;
	}
}
